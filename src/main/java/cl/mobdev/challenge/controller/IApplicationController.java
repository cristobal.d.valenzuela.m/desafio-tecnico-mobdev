package cl.mobdev.challenge.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import cl.mobdev.challenge.exception.ApplicationException;
import cl.mobdev.challenge.in.DataIn;
import cl.mobdev.challenge.out.Salida;
import io.swagger.annotations.ApiOperation;

public interface IApplicationController {

	/**
	 * Metodo que retorna un Personaje en base a un ID
	 * @param entrada
	 * @return
	 */
	@ApiOperation(value = "Datos Personaje", notes = "Metodo que retorna los datos de un personaje")
	@PostMapping(value = "/personaje", produces = MediaType.APPLICATION_JSON_VALUE)
	public Salida obtieneDatosPersonaje(@RequestBody DataIn entrada) throws ApplicationException;

}
