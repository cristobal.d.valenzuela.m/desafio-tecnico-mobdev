package cl.mobdev.challenge.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import cl.mobdev.challenge.exception.ApplicationException;
import cl.mobdev.challenge.out.Salida;

@ControllerAdvice
public class ApplicationExceptionHandler {
	@ExceptionHandler(value = { ApplicationException.class })
	protected ResponseEntity<Salida> handleConflict(ApplicationException ex, WebRequest request) {
		return new ResponseEntity<Salida>(new Salida(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage()), HttpStatus.OK);
	}
	
	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<Salida> handleConflict(Exception ex, WebRequest request) {
		return new ResponseEntity<Salida>(new Salida(HttpStatus.INTERNAL_SERVER_ERROR, "Error no controlado"), HttpStatus.OK);
	}
}
