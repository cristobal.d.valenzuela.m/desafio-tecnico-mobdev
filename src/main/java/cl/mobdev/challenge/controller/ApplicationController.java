package cl.mobdev.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cl.mobdev.challenge.exception.ApplicationException;
import cl.mobdev.challenge.in.DataIn;
import cl.mobdev.challenge.out.Personaje;
import cl.mobdev.challenge.out.Salida;
import cl.mobdev.challenge.services.IApplicationServices;

@RestController
public class ApplicationController implements IApplicationController {

	@Autowired
	private IApplicationServices applicationServices;

	@Override
	public Salida obtieneDatosPersonaje(@RequestBody DataIn entrada) throws ApplicationException {
		Personaje personaje = applicationServices.obtieneDatosPersonaje(entrada.getIdPersonaje());
		return new Salida(HttpStatus.OK, personaje);
	}
}
