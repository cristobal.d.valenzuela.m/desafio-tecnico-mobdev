package cl.mobdev.challenge.out;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(value = Include.NON_NULL)
public class Origen {

	private String name;
	private String url;
	private String dimension;
	private List<String> residents;
}
