package cl.mobdev.challenge.out;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(value = Include.NON_NULL)
public class Personaje {

	private int id;
	private String name;
	private String status;
	private String species;
	private String type;
	private int episode_count;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private List<String> episode;
	private Origen origin;	
}
