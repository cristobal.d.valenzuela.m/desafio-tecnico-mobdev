package cl.mobdev.challenge.out;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(value = Include.NON_NULL)
public class Salida {

	private int codigo;
	private String mensaje;
	private Object datos;

	public Salida(HttpStatus status, String mensaje) {
		this.codigo = status.value();
		this.mensaje = mensaje;
	}

	public Salida(HttpStatus status, Object datos) {
		this.codigo = status.value();
		this.datos = datos;
	}
}
