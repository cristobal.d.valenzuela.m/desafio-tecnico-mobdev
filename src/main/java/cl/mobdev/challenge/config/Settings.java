package cl.mobdev.challenge.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class Settings {

	@Value(value = "${url.api.rickandmorty.character}")
	private String urlRickAndMortyCharacter;
	@Value(value = "${error.obtener.info.personaje}")
	private String errorPersonajeSinInfo;
	@Value(value = "${error.obtener.origen.personaje}")
	private String errorPersonajeSinOrigen;
	@Value(value = "${origen.sin.informacion}")
	private String origenSinInformacion;
}
