package cl.mobdev.challenge.services;

import cl.mobdev.challenge.exception.ApplicationException;
import cl.mobdev.challenge.out.Personaje;

public interface IApplicationServices {
	
	public Personaje obtieneDatosPersonaje(int idPersonaje) throws ApplicationException ;
}
