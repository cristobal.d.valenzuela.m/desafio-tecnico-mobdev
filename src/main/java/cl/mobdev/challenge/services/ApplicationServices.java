package cl.mobdev.challenge.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import cl.mobdev.challenge.config.Settings;
import cl.mobdev.challenge.exception.ApplicationException;
import cl.mobdev.challenge.out.Origen;
import cl.mobdev.challenge.out.Personaje;

@Service
public class ApplicationServices implements IApplicationServices {

	@Autowired
	private Settings settings;

	@Bean
	public RestTemplate obtieneRestTemplate() {
		return new RestTemplate();
	}

	@Override
	public Personaje obtieneDatosPersonaje(int idPersonaje) throws ApplicationException {
		try {
			ResponseEntity<Personaje> respuesta = obtieneRestTemplate()
					.getForEntity(settings.getUrlRickAndMortyCharacter() + idPersonaje, Personaje.class);
			Personaje personaje = respuesta.getBody();
			personaje.setEpisode_count(personaje.getEpisode().size());
			obtieneOrigenPersonaje(personaje);
			return personaje;
		} catch (RestClientResponseException e) {
			throw new ApplicationException(settings.getErrorPersonajeSinInfo(), e);
		}
	}

	private void obtieneOrigenPersonaje(Personaje personaje) throws ApplicationException {
		if(!personaje.getOrigin().getUrl().isEmpty()){
			try {
				ResponseEntity<Origen> respuesta = obtieneRestTemplate().getForEntity(personaje.getOrigin().getUrl(),
						Origen.class);
				personaje.setOrigin(respuesta.getBody());
			} catch (RestClientResponseException e) {
				throw new ApplicationException(String.format(settings.getErrorPersonajeSinOrigen(), personaje.getName()), e);
			}
		} else {
			Origen sinInfo = new Origen();
			sinInfo.setName(settings.getOrigenSinInformacion());
			personaje.setOrigin(sinInfo);
		}
	}

}
