package cl.mobdev.challenge.exception;

public class ApplicationException extends Exception {

	private static final long serialVersionUID = -1577493075351345247L;

	public ApplicationException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

}
