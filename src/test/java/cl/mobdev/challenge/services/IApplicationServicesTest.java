package cl.mobdev.challenge.services;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import cl.mobdev.challenge.exception.ApplicationException;
import cl.mobdev.challenge.out.Personaje;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IApplicationServicesTest {

	private static final int ID_PERSONAJE = 1;
	private static final int ID_PERSONAJE_ERROR = 0;

	@Mock
	private RestTemplate restTemplate;

	@Autowired
	private ApplicationServices service;

	@Test
	public void testObtieneDatosPersonaje() throws ApplicationException {
		Personaje personaje = service.obtieneDatosPersonaje(ID_PERSONAJE);
		assertNotNull(personaje);
	}

	@Test(expected = ApplicationException.class)
	public void testObtieneDatosPersonajeError() throws ApplicationException {
		service.obtieneDatosPersonaje(ID_PERSONAJE_ERROR);
	}

}
